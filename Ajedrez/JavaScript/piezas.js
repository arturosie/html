function piezas(){          //Main function to load the figures on the board

    pawn();             //Upload "pawn" figures on the board
    tower();            //Upload "tower" figures on the board
    horse();            //Upload "horse" figures on the board
    bishop();           //Upload "bishop" figures on the board
    queen();            //Upload "queen" figures on the board
    king();             //Upload "king" figures on the board

    draw.innerHTML=" ";
    pintapiezas();          //Put figures on the board(Reference: main.js:86)

}

function pawn(){        //Function to upload "pawn" figures on the board

    var ID = 9;

    for(var i=1; i<2; i++){
        for( var j=0; j<tamanio; j++){
            if((i+j) % 2 == 0){                                                                                                 //Check where is the new position of the figure
                tablero[i][j] = {Col: i, Fila: j, ID: ID, Img: '<img src="Black_PawnW.png" onclick="dondemover(event)"/>'};     //Insert the new values ​​into the box
                i = 6;
                tablero[i][j] = {Col: i, Fila: j, ID: -ID, Img: '<img src="White_PawnB.png" onclick="dondemover(event)"/>'};    //Insert the new values ​​into the box
                i = 1;
            }
            else{
                tablero[i][j] = {Col: i, Fila: j, ID: ID, Img: '<img src="Black_PawnB.png" onclick="dondemover(event)"/>'};     //Insert the new values ​​into the box
                i = 6;
                tablero[i][j] = {Col: i, Fila: j, ID: -ID, Img: '<img src="White_PawnW.png" onclick="dondemover(event)"/>'};    //Insert the new values ​​into the box
                i = 1;
            }
            ID++;
        }
    }

}

function tower(){           //Function to upload "tower" figures on the board
    tablero[0][0] = {Col: 0, Fila: 0, ID: 1, Img: '<img src="Black_TowerW.png" onclick="dondemover(event)"/>'};         //Black towers on white box
    tablero[0][7] = {Col: 0, Fila: 7, ID: 8, Img: '<img src="Black_TowerB.png" onclick="dondemover(event)"/>'};         //Black towers on black box
    tablero[7][0] = {Col: 7, Fila: 0, ID: -1, Img: '<img src="White_TowerB.png" onclick="dondemover(event)"/>'};        //White towers on white box
    tablero[7][7] = {Col: 7, Fila: 7, ID: -8, Img: '<img src="White_TowerW.png" onclick="dondemover(event)"/>'};        //White towers on black box
}

function horse(){           //Function to upload "horse" figures on the board
    tablero[0][1] = {Col: 0, Fila: 1, ID: 2, Img: '<img src="Black_HorseB.png" onclick="dondemover(event)"/>'};         //Black horse on black box
    tablero[0][6] = {Col: 0, Fila: 6, ID: 7, Img: '<img src="Black_HorseW.png" onclick="dondemover(event)"/>'};         //Black horse on white box
    tablero[7][1] = {Col: 7, Fila: 1, ID: -2, Img: '<img src="White_HorseW.png" onclick="dondemover(event)"/>'};        //White towers on white box
    tablero[7][6] = {Col: 7, Fila: 6, ID: -7, Img: '<img src="White_HorseB.png" onclick="dondemover(event)"/>'};        //White towers in black box
}

function bishop(){          //Function to upload "bishop" figures on the board
    tablero[0][2] = {Col: 0, Fila: 2, ID: 3, Img: '<img src="Black_BishopW.png" onclick="dondemover(event)"/>'};        //Black bishop in white box
    tablero[0][5] = {Col: 0, Fila: 5, ID: 6, Img: '<img src="Black_BishopB.png" onclick="dondemover(event)"/>'};        //Black bishop in black box
    tablero[7][2] = {Col: 7, Fila: 2, ID: -3, Img: '<img src="White_BishopB.png" onclick="dondemover(event)"/>'};       //White bishop in black box
    tablero[7][5] = {Col: 7, Fila: 5, ID: -6, Img: '<img src="White_BishopW.png" onclick="dondemover(event)"/>'};       //White bishop in white box
}

function queen(){           //Function to upload "queen" figures on the board
    tablero[0][4] = {Col: 0, Fila: 4, ID: 5, Img: '<img src="Black_QueenW.png" onclick="dondemover(event)"/>'};         //Black queen in white box
    tablero[7][4] = {Col: 7, Fila: 4, ID: -5, Img: '<img src="White_QueenB.png" onclick="dondemover(event)"/>'};        //White queen in black box
}

function king(){            //Function to upload "king" figures on the board
    tablero[0][3] = {Col: 0, Fila: 3, ID: 4, Img: '<img src="Black_KingB.png" onclick="dondemover(event)"/>'};          //Black king in black box
    tablero[7][3] = {Col: 7, Fila: 4, ID: -4, Img: '<img src="White_KingW.png" onclick="dondemover(event)"/>'};         //Black king in white box
}