function borrarpiezas(){		//Function to remove the figures from the board
    draw.innerHTML=" ";

    cargatablero();			//Load the board (Reference: main.js:64)
    pintatablero();			//Put figures on the board (Reference: main.js:76)
}

function limpiamovimiento(){	//Function to clean the movement made
    borrarpiezas();				//Remove the figures from the board (References: utiles.js:1)
    piezas();					//Put the figures on the board
}

function remplaza(){				//Function to replace the box of the figure that we have moved by its color (white or black)

 //   alert("Row: " + row + " Column: " + column +					//Test mode
 //       " DondeID: " + DondeID + " FichaID: " + FichaID)
    /*for(var i=0; i<tamanio; i++){
        for(var j=0; j<tamanio; j++){
            IDFicha = tablero[i][j].ID;
            if(IDFicha == DondeID){
                ColFicha = tablero[i][j].Col;
                FilaFicha = tablero[i][j].Fila;															TEST MODE 
                if((ColFicha + FilaFicha) % 2 != 0)															You can move to any address but don't replace the card with black or white
                    tablero[i][j]={Col: column, Fila: row, ID: 99, Img: '<img src="Black.png"/>'};
                else
                    tablero[i][j]={Col: column, Fila: row, ID: 98, Img: '<img src="White.png"/>'};
            }
            if(IDFicha == DondeID)
                break;
        }
        if(IDFicha == DondeID)
            break;
    }*/

    for(var i=0; i<tamanio; i++){
        for(var j=0; j<tamanio; j++){
            IDFicha = tablero[i][j].ID;																	//TEST MODE
            if(IDFicha == FichaID){																			//You can move down or right but replace the card with black or white
                ColFicha = tablero[i][j].Col;
                FilaFicha = tablero[i][j].Fila;
                if((ColFicha + FilaFicha) % 2 != 0)
                    tablero[i][j]={Col: column, Fila: row, ID: 99, Img: '<img src="Black.png"/>'};
                else
                    tablero[i][j]={Col: column, Fila: row, ID: 98, Img: '<img src="White.png"/>'};
            }
            if(IDFicha == FichaID)
                break;
        }
        if(IDFicha == FichaID)
            break;
    }


    borrarred();        //Function to clear the boxes where there was a threat to a figure (Reference: utiles.js:59)
    borrarblue();       //Function to clear the boxes where a movement can be made (Reference: utiles.js:77)

}

function borrarred(){		//Function to clear the boxes where there was a threat to a figure

    var inc = 0;

    for(var i=0; i<tamanio; i++){
        for(var j=0; j<tamanio; j++){
            IDFicha = tablero[i][j].ID;
            if(IDFicha >= 200){						//Search all the boxes with the ID greater than or equal to 200 (only the red boxes)
                ColFicha = tablero[i][j].Col;			//Stores column of the box
                FilaFicha = tablero[i][j].Fila;			//Stores row of the box
                tablero[i][j] = recuperaficha[inc];		//Copy the data of the variable "recover tab" in the box
                inc++;
            }
        }
    }

}

function borrarblue(){			//Function to clear the boxes where a movement can be made

    for(var i=0; i<tamanio; i++){
        for(var j=0; j<tamanio; j++){
            IDFicha = tablero[i][j].ID;
            if(IDFicha >= 100){																			//Search all the boxes with the ID greater than or equal to 100 (only the blue boxes)
                ColFicha = tablero[i][j].Col;															//Stores column of the box
                FilaFicha = tablero[i][j].Fila;															//Stores row of the box
                if((ColFicha + FilaFicha) % 2 != 0)														//Check where is the new position of the figure
                    tablero[i][j]={Col: column, Fila: row, ID: 99, Img: '<img src="Black.png"/>'};
                else
                    tablero[i][j]={Col: column, Fila: row, ID: 98, Img: '<img src="White.png"/>'};
            }
        }
    }

    draw.innerHTML=" ";
    muevepiezas();			//Draws the figure in the box where we want to move it (Reference: main.js:96)
}