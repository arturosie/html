
//The figures can't eat because they are all black and you can't eat figures from the same team

function dibujarpiezas(ev){     //Create the data of the figures in an array and put them on the screen

    colocarpieza[0] = {Col: 99, Fila: 99, ID: 50, Img: '<img src="Black_PawnW.png"  onclick="colocarpiezatablero(event)"/>'};       //Pawn
    colocarpieza[1] = {Col: 99, Fila: 99, ID: 51, Img: '<img src="Black_TowerW.png" onclick="colocarpiezatablero(event)"/>'};       //Tower
    colocarpieza[2] = {Col: 99, Fila: 99, ID: 52, Img: '<img src="Black_HorseW.png" onclick="colocarpiezatablero(event)"/>'};       //Horse
    colocarpieza[3] = {Col: 99, Fila: 99, ID: 53, Img: '<img src="Black_BishopW.png" onclick="colocarpiezatablero(event)"/>'};      //Bishop
    colocarpieza[4] = {Col: 99, Fila: 99, ID: 54, Img: '<img src="Black_QueenW.png" onclick="colocarpiezatablero(event)"/>'};       //Queen
    colocarpieza[5] = {Col: 99, Fila: 99, ID: 55, Img: '<img src="Black_KingW.png" onclick="colocarpiezatablero(event)"/>'};        //King

    colocar.innerHTML = " ";

    for(var i=0; i < 6; i++){                                   //Put figures to place on the screen
        colocar.innerHTML += colocarpieza[i].Img+"&nbsp";
        if(i==2)
            colocar.innerHTML += "<br>";
    }
}

function colocarpiezatablero(ev){           //Function that allows to place the selected figure on the board

    borrarred();        //Function to clear the boxes where there was a threat to a figure (Reference: utiles.js:59)
    borrarblue();       //Function to clear the boxes where a movement can be made (Reference: utiles.js:77)

    //Check what figure we have selected
    var fila  = parseInt ((parseInt(ev.pageX) - 660 ) / 60)
    var col = parseInt((parseInt(ev.pageY) - 60 ) / 60)

    if(col == 0)
        FichaID = colocarpieza[(fila-1)+col].ID;
    if(col == 1)
        FichaID = colocarpieza[(fila+1)+col].ID;
    //End check

    var i=0;
    for(column=0; column < tamanio; column++){                                      //Check available boxes to place the figure
        for(row=0; row < tamanio; row++){
            if(tablero[column][row].ID == 98 || tablero[column][row].ID == 99){
                tablero[column][row]={Col: column, Fila: row, ID: 100+i, Img: '<img src="Blue.png" onclick="colocarpiezas(event)"/>'};
                i++;
            }
        }
    }

    draw.innerHTML=" ";

    muevepiezas();          //Draws the figure in the box where we want to move it (Reference: main.js:96)
}

function colocarpiezas(ev){
    row  = parseInt ((parseInt(ev.pageX) - 7 ) / 60)        //Row of the box where the selected figure will move
    column = parseInt((parseInt(ev.pageY) - 60 ) / 60)      //Column of the box where the selected figure will move
    DondeID = tablero[column][row].ID;                      //ID of the box where the selected figure will move

    //Put pawn on the board
    if(FichaID == 50){
        if(colocarsumapawn > 7) 
            alert("HAS SUPERADO EL MAXIMO DE PEONES EN EL TABLERO");
        else{
            if((column + row) % 2 != 0)
                tablero[column][row]={Col: column, Fila: row, ID: 9+colocarsumapawn, Img: '<img src="Black_PawnB.png" onclick="dondemover(event)"/>'};
            else
                tablero[column][row]={Col: column, Fila: row, ID: 9+colocarsumapawn, Img: '<img src="Black_PawnW.png" onclick="dondemover(event)"/>'};

        }
        colocarsumapawn++;
    }

    //Put Tower on the board
    if(FichaID == 51){
        if(colocarsumatower > 8)
            alert("HAS SUPERADO EL MAXIMO DE TORRES EN EL TABLERO");
        else{
            if((column + row) % 2 != 0)
                tablero[column][row]={Col: column, Fila: row, ID: 1+colocarsumatower, Img: '<img src="Black_TowerB.png" onclick="dondemover(event)"/>'};
            else
                tablero[column][row]={Col: column, Fila: row, ID: 1+colocarsumatower, Img: '<img src="Black_TowerW.png" onclick="dondemover(event)"/>'};
        }
        colocarsumatower+=7;
    }

    //Put Horse on the board
    if(FichaID == 52){
        if(colocarsumahorse > 7)
            alert("HAS SUPERADO EL MAXIMO DE CABALLOS EN EL TABLERO");
        else{
            if((column + row) % 2 != 0)
                tablero[column][row]={Col: column, Fila: row, ID: 2+colocarsumahorse, Img: '<img src="Black_HorseB.png" onclick="dondemover(event)"/>'};
            else
                tablero[column][row]={Col: column, Fila: row, ID: 2+colocarsumahorse, Img: '<img src="Black_HorseW.png" onclick="dondemover(event)"/>'};
        }
        colocarsumahorse+=5;
    }

    //Put bishop on the board
    if(FichaID == 53){
        if(colocarsumabishop > 4)
            alert("HAS SUPERADO EL MAXIMO DE ALFILES EN EL TABLERO");
        else{
            if((column + row) % 2 != 0)
                tablero[column][row]={Col: column, Fila: row, ID: 3+colocarsumabishop, Img: '<img src="Black_BishopB.png" onclick="dondemover(event)"/>'};
            else
                tablero[column][row]={Col: column, Fila: row, ID: 3+colocarsumabishop, Img: '<img src="Black_BishopW.png" onclick="dondemover(event)"/>'};
        }
        colocarsumabishop+=3;
    }

    //Put queen on the board
    if(FichaID == 54){
        if(colocarsumaqueen == 2)
            alert("HAS SUPERADO EL MAXIMO DE REINAS EN EL TABLERO");
        else{
            if((column + row) % 2 != 0)
                tablero[column][row]={Col: column, Fila: row, ID: 5+colocarsumaqueen, Img: '<img src="Black_QueenB.png" onclick="dondemover(event)"/>'};
            else
                tablero[column][row]={Col: column, Fila: row, ID: 5+colocarsumaqueen, Img: '<img src="Black_QueenW.png" onclick="dondemover(event)"/>'};
        }
        colocarsumaqueen=2;
    }

    //Put king on the board
    if(FichaID == 55){
        if(colocarsumaking == 1)
            alert("HAS SUPERADO EL MAXIMO DE REYES EN EL TABLERO");
        else{
            if((column + row) % 2 != 0)
                tablero[column][row]={Col: column, Fila: row, ID: 4+colocarsumaking, Img: '<img src="Black_KingB.png" onclick="dondemover(event)"/>'};
            else
                tablero[column][row]={Col: column, Fila: row, ID: 4+colocarsumaking, Img: '<img src="Black_KingW.png" onclick="dondemover(event)"/>'};
        }
        colocarsumaking=1;
    }

    remplaza();                             //Replace the box of the figure that we have moved by its color (black or white) (References: utiles.js:13)
}