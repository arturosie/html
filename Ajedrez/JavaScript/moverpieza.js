function dondemover(ev){            //Main function to move the figures
    draw.innerHTML=" ";

    row  = parseInt ((parseInt(ev.pageX) - 7 ) / 60)           //Row of the figure we want to move
    column = parseInt((parseInt(ev.pageY) - 60 ) / 60)         //Column of the figure we want to move

    FichaID = tablero[column][row].ID;          //ID of the figure we want to move

    //    alert("ROW: " + row + " Column: " + column +      //Test mode
    //        " FichaID: " + FichaID)

    pintatablero();         //Put figures on the board (Reference: main.js:76)
    dondepieza();           //Put that movement can realize the piece (Reference: moverpieza.js:16)
}

function dondepieza(){      //Function to indicate where the selected figure can perform the movement

    borrarred();        //Function to clear the boxes where there was a threat to a figure (Reference: utiles.js:59)
    borrarblue();       //Function to clear the boxes where a movement can be made (Reference: utiles.js:77)

    //Black pawn
    if(FichaID == 9 || FichaID == 10 || FichaID == 11 || FichaID == 12 || FichaID == 13 || FichaID == 14 || FichaID == 15 || FichaID == 16){
        var nomovimiento = 0;                   //Variable that indicates if a figure can make a movement

        if(tablero[column+1][row].ID == 98 || tablero[column+1][row].ID == 99){                                                     //Check if the box where the movement would be made isn't busy
            tablero[column+1][row]={Col: column+1, Fila: row, ID: 101, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'}; //Insert the new values ​​into the box
            if(tablero[column][row].Col == 1){                                                                                      //Check if the column is 1
                if(tablero[column+2][row].ID == 98 || tablero[column+2][row].ID == 99)                                              //Check if the box where the movement would be made isn't busy
                    tablero[column+2][row]={Col: column+2, Fila: row, ID: 102, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'}; //Insert the new values ​​into the box
            }
        }
        else                //If a movement can't be made, the variable "nomovimiento" increases
            nomovimiento++;

        //Movements to attack
        if(row != 7){                                                                               //Check if the figure isn't in the last row
            if((tablero[column+1][row+1].ID != 98 && tablero[column+1][row+1].ID < 0)){             //Check if the box where the movement would be made is busy
                if(tablero[column+1][row+1].ID != 99 || tablero[column+1][row+1].ID != 98 ){
                    recuperaficha[1] = tablero[column+1][row+1];                                    //Stores data of the attacked figure in the variable "recuperaficha"
                    tablero[column+1][row+1]={Col: column+1, Fila: row+1, ID: 201, Img: '<img src="Red.png" onclick="cambiopiezas(event)"/>'}; //Insert the new values ​​into the box
                }
                else       //If a movement can't be made, the variable "nomovimiento" increases
                    nomovimiento++;
            }
        }
        if(row != 0){                                                                               //Check if the figure isn't in the first row
            if((tablero[column+1][row-1].ID != 98 && tablero[column+1][row-1].ID < 0)){             //Check if the box where the movement would be made is busy
                if(tablero[column+1][row-1].ID != 99 || tablero[column+1][row+1].ID != 98){
                    recuperaficha[0] = tablero[column+1][row-1];                                    //Stores data of the attacked figure in the variable "recuperaficha"
                    tablero[column+1][row-1]={Col: column+1, Fila: row-1, ID: 200, Img: '<img src="Red.png" onclick="cambiopiezas(event)"/>'}; //Insert the new values ​​into the box
                }
                else        //If a movement can't be made, the variable "nomovimiento" increases
                    nomovimiento++;
            }
        }

        if(nomovimiento>2)                                              //If the value of the variable "nominative" is different from "0" it shows an alert
            alert("Esta figura no tiene ningun movimiento disponible")
    }

    //White pawns
    if(FichaID == -9 || FichaID == -10 || FichaID == -11 || FichaID == -12 || FichaID == -13 || FichaID == -14 || FichaID == -15 || FichaID == -16){

       // tablero[column-1][row]={Col: column-1, Fila: row, ID: 101, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};       Test mode

        if(tablero[column][row].Col == 6){
            if(tablero[column-2][row].ID == 98 || tablero[column-2][row].ID == 99)
                tablero[column-2][row]={Col: column-2, Fila: row, ID: 102, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
        }

        if(tablero[column-1][row].ID == 98 || tablero[column-1][row].ID == 99)
            tablero[column-1][row]={Col: column-1, Fila: row, ID: 101, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};

        if(row != 7){
            if((tablero[column-1][row+1].ID != 98 && tablero[column-1][row+1].ID < 0)){
                if(tablero[column-1][row+1].ID != 99)
                    tablero[column-1][row+1]={Col: column-1, Fila: row+1, ID: 101, Img: '<img src="Red.png" onclick="cambiopiezas(event)"/>'};
            }
        }

        if(row != 0){
            if((tablero[column-1][row-1].ID != 98 && tablero[column-1][row-1].ID < 0)){
                if(tablero[column-1][row-1].ID != 99)
                    tablero[column-1][row-1]={Col: column-1, Fila: row-1, ID: 101, Img: '<img src="Red.png" onclick="cambiopiezas(event)"/>'};
            }
        }
    }

    //Towers and queen
    if(FichaID == 1 || FichaID == -8 || FichaID == -1 || FichaID == 8 || FichaID == 5 || FichaID == -5){
        var i=1,
            nomovimiento = 0;

        if(column+i != 8){                  //Check if the figure isn't in the last column
            while(column+i != 8){
                if(tablero[column+i][row].ID == 98 || tablero[column+i][row].ID == 99){             //Check if the box where the movement would be made isn't busy
                    tablero[column+i][row] = {Col: column+i, Fila: row, ID: 100+i, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'}; //Insert the new values ​​into the box
                    i++;
                }
                else{
                    nomovimiento++;
                    break;
                }
            }
        }
        else
            nomovimiento++;

        i=1;

        if(column-i >= 0){              //Check if the figure isn't in the first column
            while(column-i >= 0){
                if(tablero[column-i][row].ID == 98 || tablero[column-i][row].ID == 99){             //Check if the box where the movement would be made isn't busy
                    tablero[column-i][row] = {Col: column-i, Fila: row, ID: 106+i, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'}; //Insert the new values ​​into the box
                    i++;
                }
                else{
                    nomovimiento++;
                    break;
                }
            }
        }
        else
            nomovimiento++;

        i=1;

        if(row-i >= 0){                 //Check if the figure isn't in the first row
            while(row-i >= 0){
                if(tablero[column][row-i].ID == 98 || tablero[column][row-i].ID == 99){            //Check if the box where the movement would be made isn't busy
                    tablero[column][row-i] = {Col: column, Fila: row-i, ID: 114+i, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'}; //Insert the new values ​​into the box
                    i++;
                }
                else{
                    nomovimiento++;
                    break;
                }
            }
        }
        else
            nomovimiento++;

        i=1;

        if(row+i != 8){             //Check if the figure isn't in the last row
            while(row+i != 8){
                if(tablero[column][row+i].ID == 98 || tablero[column][row+i].ID == 99){         //Check if the box where the movement would be made isn't busy
                    tablero[column][row+i] = {Col: column, Fila: row+i, ID:121+i, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'}; //Insert the new values ​​into the box
                    i++;
                }
                else{
                    nomovimiento++;
                    break;
                }
            }
        }
        else
            nomovimiento++;

        if(nomovimiento == 4)                                           //If the value of the variable "nominative" is different from "0" it shows an alert
            alert("Esta figura no tiene ningun movimiento disponible")
    }

    //Horse and queen
    if(FichaID == 2 || FichaID == -2 || FichaID == -7 || FichaID == 7 || FichaID == 5 || FichaID == -5){
        var nomovimiento = 0;

        if(row < 7 && column > 1)
            if(tablero[column-2][row+1].ID == 98 || tablero[column-2][row+1].ID == 99)
                tablero[column-2][row+1] = {Col: column-2, Fila: row+1, ID:100, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
            else
            nomovimiento++;
        else
            nomovimiento++;

        if(row < 6 && column > 0)
            if(tablero[column-1][row+2].ID == 98 || tablero[column-1][row+2].ID == 99)
                tablero[column-1][row+2] = {Col: column-1, Fila: row+2, ID:101, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
            else
            nomovimiento++;
        else
            nomovimiento++;

        if(row < 6 && column < 7)
            if(tablero[column+1][row+2].ID == 98 || tablero[column+1][row+2].ID == 99)
                tablero[column+1][row+2] = {Col: column+1, Fila: row+2, ID:102, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
            else
            nomovimiento++;
        else
            nomovimiento++;

        if(row < 7 && column < 6)
            if(tablero[column+2][row+1].ID == 98 || tablero[column+2][row+1].ID == 99)
                tablero[column+2][row+1] = {Col: column+2, Fila: row+1, ID:103, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
            else
            nomovimiento++;
        else
            nomovimiento++;

        if(row > 0 && column > 1)
            if(tablero[column-2][row-1].ID == 98 || tablero[column-2][row-1].ID == 99)
                tablero[column-2][row-1] = {Col: column-2, Fila: row-1, ID:104, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
            else
            nomovimiento++;
        else
            nomovimiento++;

        if(row > 1 && column > 0)
            if(tablero[column-1][row-2].ID == 98 || tablero[column-1][row-2].ID == 99)
                tablero[column-1][row-2] = {Col: column-1, Fila: row-2, ID:105, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
            else
            nomovimiento++;
        else
            nomovimiento++;

        if(row > 1 && column < 7)
            if(tablero[column+1][row-2].ID == 98 || tablero[column+1][row-2].ID == 99)
                tablero[column+1][row-2] = {Col: column+1, Fila: row-2, ID:106, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
            else
            nomovimiento++;
        else
            nomovimiento++;

        if(row > 0 && column < 6)
            if(tablero[column+2][row-1].ID == 98 || tablero[column+2][row-1].ID == 99)
                tablero[column+2][row-1] = {Col: column+2, Fila: row-1, ID:107, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
            else
            nomovimiento++;
        else
            nomovimiento++;

        if(nomovimiento == 8)
            alert("Esta figura no tiene ningun movimiento disponible")
    }

    //Bishop and queen
    if(FichaID == 3 || FichaID == -3 || FichaID == -6 || FichaID == 6 || FichaID == 5 || FichaID == -5){
        var i=1,
            nomovimiento = 0;

        if (column+i != 8 && row+i != 8){
            while(column+i != 8 && row+i != 8){
                if(tablero[column+i][row+i].ID == 98 || tablero[column+i][row+i].ID == 99){
                    tablero[column+i][row+i] = {Col: column+i, Fila: row+i, ID: 100+i, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
                    i++;
                }
                else{
                    nomovimiento++;
                    break;
                }
            }
        }
        else
            nomovimiento++;

        i=1;

        if(column+i != 8 && row-i >= 0){
            while(column+i != 8 && row-i >= 0){
                if(tablero[column+i][row-i].ID == 98 || tablero[column+i][row-i].ID == 99){
                    tablero[column+i][row-i] = {Col: column+i, Fila: row-i, ID: 100+i, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
                    i++;
                }
                else{
                    nomovimiento++;
                    break;
                }
            }
        }
        else
            nomovimiento++;

        i=1;

        if(column-i >= 0 && row+i != 8){
            while(column-i >= 0 && row+i != 8){
                if(tablero[column-i][row+i].ID == 98 || tablero[column-i][row+i].ID == 99){
                    tablero[column-i][row+i] = {Col: column-i, Fila: row+i, ID: 106+i, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
                    i++;
                }
                else{
                    nomovimiento++;
                    break;
                }
            }
        }
        else
            nomovimiento++;

        i=1;

        if(column-i >= 0 && row-i >= 0){
            while(column-i >= 0 && row-i >= 0){
                if(tablero[column-i][row-i].ID == 98 || tablero[column-i][row-i].ID == 99){
                    tablero[column-i][row-i] = {Col: column-i, Fila: row-i, ID: 114+i, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
                    i++;
                }
                else{
                    nomovimiento++;
                    break;
                }
            }
        }
        else
            nomovimiento++;

        if(nomovimiento == 4)
            alert("Esta figura no tiene ningun movimiento disponible")
    }

    //King
    if(FichaID == 4 || FichaID == -4){
        var nomovimiento = 0;

        if(column+1 != 8){
            if(tablero[column+1][row].ID == 98 || tablero[column+1][row].ID == 99)
                tablero[column+1][row] = {Col: column+1, Fila: row, ID: 101, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
            else
                nomovimiento++;
        }
        else
            nomovimiento++;

        if(column-1 >= 0){
            if(tablero[column-1][row].ID == 98 || tablero[column-1][row].ID == 99)
                tablero[column-1][row] = {Col: column-1, Fila: row, ID: 102, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
            else
                nomovimiento++;
        }
        else
            nomovimiento++;

        if(row-1 >= 0){
            if(tablero[column][row-1].ID == 98 || tablero[column][row-1].ID == 99)
                tablero[column][row-1] = {Col: column, Fila: row-1, ID: 103, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
            else
                nomovimiento++;
        }
        else
            nomovimiento++;

        if(row+1 != 8){
            if(tablero[column][row+1].ID == 98 || tablero[column][row+1].ID == 99)
                tablero[column][row+1] = {Col: column, Fila: row+1, ID:104, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
            else
                nomovimiento++;
        }
        else
            nomovimiento++;

        if(column+1 != 8 && row+1 != 8){
            if(tablero[column+1][row+1].ID == 98 || tablero[column+1][row+1].ID == 99)
                tablero[column+1][row+1] = {Col: column+1, Fila: row+1, ID: 105, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
            else
                nomovimiento++;
        }
        else
            nomovimiento++;

        if(column+1 != 8 && row-1 >= 0){
            if(tablero[column+1][row-1].ID == 98 || tablero[column+1][row-1].ID == 99)
                tablero[column+1][row-1] = {Col: column+1, Fila: row-1, ID: 106, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
            else
                nomovimiento++;
        }
        else
            nomovimiento++;

        if(column-1 >= 0 && row+1 != 8){
            if(tablero[column-1][row+1].ID == 98 || tablero[column-1][row+1].ID == 99)
                tablero[column-1][row+1] = {Col: column-1, Fila: row+1, ID: 107, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
            else
                nomovimiento++;
        }
        else
            nomovimiento++;

        if(column-1 >= 0 && row-1 >= 0){
            if(tablero[column-1][row-1].ID == 98 || tablero[column-1][row-1].ID == 99)
                tablero[column-1][row-1] = {Col: column-1, Fila: row-1, ID: 108, Img: '<img src="Blue.png" onclick="cambiopiezas(event)"/>'};
            else
                nomovimiento++;
        }
        else
            nomovimiento++;

        if(nomovimiento == 8)
            alert("Esta figura no tiene ningun movimiento disponible")
    }


    draw.innerHTML=" ";
    muevepiezas();          //Draws the figure in the box where we want to move it (Reference: main.js:96)

}

function cambiopiezas(ev){                          //Function to move the selected figure to the selected place

    row  = parseInt ((parseInt(ev.pageX) - 7 ) / 60)        //Row of the box where the selected figure will move
    column = parseInt((parseInt(ev.pageY) - 60 ) / 60)      //Column of the box where the selected figure will move
    DondeID = tablero[column][row].ID;                      //ID of the box where the selected figure will move

  //  alert("Row: " + row + " Column: " + column +          //Test mode
  //      " DondeID: " + DondeID + " FichaID: " + FichaID)

    //Put a pawn where we want to move it
    if(FichaID == 9 || FichaID == 10 || FichaID == 11 || FichaID == 12 || FichaID == 13 || FichaID == 14 || FichaID == 15 || FichaID == 16 ||
        FichaID == -9 || FichaID == -10 || FichaID == -11 || FichaID == -12 || FichaID == -13 || FichaID == -14 || FichaID == -15 || FichaID == -16){
        if((column + row) % 2 != 0){                                                                                                                  //Check where is the new position of the figure
            if(FichaID == 9 || FichaID == 10 || FichaID == 11 || FichaID == 12 || FichaID == 13 || FichaID == 14 || FichaID == 15 || FichaID == 16)     //Black pawn
                tablero[column][row]={Col: column, Fila: row, ID: FichaID, Img: '<img src="Black_PawnB.png" onclick="dondemover(event)"/>'};            //Insert the new values ​​into the box

            if(FichaID == -9 || FichaID == -10 || FichaID == -11 || FichaID == -12 || FichaID == -13 || FichaID == -14 || FichaID == -15 || FichaID == -16) //White pawn
                tablero[column][row]={Col: column, Fila: row, ID: FichaID, Img: '<img src="White_PawnB.png" onclick="dondemover(event)"/>'};                //Insert the new values ​​into the box
        }

        else{
            if(FichaID == 9 || FichaID == 10 || FichaID == 11 || FichaID == 12 || FichaID == 13 || FichaID == 14 || FichaID == 15 || FichaID == 16)
                tablero[column][row]={Col: column, Fila: row, ID: FichaID, Img: '<img src="Black_PawnW.png" onclick="dondemover(event)"/>'};            //Insert the new values ​​into the box

            if(FichaID == -9 || FichaID == -10 || FichaID == -11 || FichaID == -12 || FichaID == -13 || FichaID == -14 || FichaID == -15 || FichaID == -16)
                tablero[column][row]={Col: column, Fila: row, ID: FichaID, Img: '<img src="White_PawnW.png" onclick="dondemover(event)"/>'};            //Insert the new values ​​into the box
        }
    }

  //  alert("Row: " + row + " Column: " + column +          //Test mode
  //      " DondeID: " + DondeID + " FichaID: " + FichaID)

    //Put a tower where we want to move it
    if(FichaID == 1 || FichaID == -8 || FichaID == -1 || FichaID == 8){
        if((column + row) % 2 != 0)                                                                                                         //Check where is the new position of the figure
            tablero[column][row]={Col: column, Fila: row, ID: FichaID, Img: '<img src="Black_TowerB.png" onclick="dondemover(event)"/>'};
        else
            tablero[column][row]={Col: column, Fila: row, ID: FichaID, Img: '<img src="Black_TowerW.png" onclick="dondemover(event)"/>'};
    }

    //Put a horse where we want to move it
    if(FichaID == 2 || FichaID == -2 || FichaID == -7 || FichaID == 7){
        if((column + row) % 2 != 0)                                                                                                         //Check where is the new position of the figure
            tablero[column][row]={Col: column, Fila: row, ID: FichaID, Img: '<img src="Black_HorseB.png" onclick="dondemover(event)"/>'};
        else
            tablero[column][row]={Col: column, Fila: row, ID: FichaID, Img: '<img src="Black_HorseW.png" onclick="dondemover(event)"/>'};
    }

    //Put a bishop where we want to move it
    if(FichaID == 3 || FichaID == -3 || FichaID == -6 || FichaID == 6){
        if((column + row) % 2 != 0)                                                                                                         //Check where is the new position of the figure
            tablero[column][row]={Col: column, Fila: row, ID: FichaID, Img: '<img src="Black_BishopB.png" onclick="dondemover(event)"/>'};
        else
            tablero[column][row]={Col: column, Fila: row, ID: FichaID, Img: '<img src="Black_BishopW.png" onclick="dondemover(event)"/>'};
    }

    //Put a queen where we want to move it
    if(FichaID == 5 || FichaID == -5){
        if((column + row) % 2 != 0)                                                                                                         //Check where is the new position of the figure
            tablero[column][row]={Col: column, Fila: row, ID: FichaID, Img: '<img src="Black_QueenB.png" onclick="dondemover(event)"/>'};
        else
            tablero[column][row]={Col: column, Fila: row, ID: FichaID, Img: '<img src="Black_QueenW.png" onclick="dondemover(event)"/>'};
    }

    //Put a king where we want to move it
    if(FichaID == 4 || FichaID == -4){
        if((column + row) % 2 != 0)                                                                                                         //Check where is the new position of the figure
            tablero[column][row]={Col: column, Fila: row, ID: FichaID, Img: '<img src="Black_KingB.png" onclick="dondemover(event)"/>'};
        else
            tablero[column][row]={Col: column, Fila: row, ID: FichaID, Img: '<img src="Black_KingW.png" onclick="dondemover(event)"/>'};
    }

    //   borrarred();                                                             //Test mode
    //   borrarblue();                                                            //Test mode
    remplaza();                          //Replace the box of the figure that we have moved by its color (black or white) (References: utiles.js:13)
}